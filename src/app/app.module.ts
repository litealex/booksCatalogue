import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import {
  InfrastructureModule
} from './infrastructure';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InfrastructureModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
