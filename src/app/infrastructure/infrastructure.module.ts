import { NgModule } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { StorageService } from './storage.service';

@NgModule({
    imports: [],
    exports: [],
    providers: [
        LocalStorageService,
        {
            provide: StorageService,
            useClass: LocalStorageService
        }
    ],
})
export class InfrastructureModule { }
