
export abstract class StorageService {
    abstract getItem<T>(key: string): T;
    abstract saveItem<T>(key: string, item: T): T;
    abstract removeItem(key: string): void;
}