import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { rendererTypeName } from '@angular/compiler';

@Injectable()
export class LocalStorageService implements StorageService {

    getItem<T>(key: string): T {
        const json = localStorage.getItem(key);
        if (json === null) {
            return null;
        }
        return JSON.parse(json) as T;
    }
    saveItem<T>(key: string, item: T): T {
        localStorage.setItem(key, JSON.stringify(item));
        return item;
    }
    removeItem(key: string): void {
        localStorage.removeItem(key);
    }
    constructor() { }
}