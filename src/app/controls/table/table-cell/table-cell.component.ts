import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'bc-table-cell',
    templateUrl: 'table-cell.component.html',
    host: {
        class: 'bc-table__cell'
    },
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class TableCellComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}