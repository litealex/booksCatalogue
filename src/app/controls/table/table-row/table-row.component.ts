import {
    Component,
    OnInit,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector: 'bc-table-row',
    templateUrl: 'table-row.component.html',
    host: {
        class: 'bc-table__row'
    },
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class TableRowComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}