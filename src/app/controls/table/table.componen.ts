import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    ContentChildren,
    QueryList,
    AfterContentInit
} from '@angular/core';
import { TableRowComponent } from './table-row';
import { TableCellComponent } from './table-cell';

@Component({
    selector: 'bc-table',
    templateUrl: 'table.component.html',
    host: {
        class: 'bc-table'
    },
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit {
  
    constructor() { }

    ngOnInit() { }
}