import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    HostBinding,
    OnChanges,
    SimpleChanges
} from '@angular/core';

const baseCssClass = 'bc-button';

@Component({
    selector: 'bc-button',
    templateUrl: 'button.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ButtonComponent implements OnInit, OnChanges {
    ngOnChanges(changes: SimpleChanges): void {
        this.setCssClass();
    }

    setCssClass() {
        this.cssClass = [
            baseCssClass,
            this.type ? `${baseCssClass}_${this.type}` : null
        ]
            .filter(x => x).join(' ')
    }

    @HostBinding('class')
    cssClass: string;
    @Input()
    type: string;
    constructor() { }

    ngOnInit() { 
        this.setCssClass();
    }
}