import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'bc-link',
    templateUrl: 'link.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        class: 'bc-link'
    }
})

export class LinkComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}