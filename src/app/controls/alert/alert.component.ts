import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'bc-alert',
    templateUrl: 'alert.component.html',
    host: {
        class: 'bc-alert'
    },
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class AlertComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}