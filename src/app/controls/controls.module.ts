import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button';
import {
    TableComponent,
    TableCellComponent,
    TableRowComponent
} from './table';
import {
    CartComponent,
    CartItemComponent
} from './cart';
import { ImageComponent } from './image';
import { ImageLoaderComponent } from './image-loader'
import { LinkComponent } from './link';
import { AlertComponent } from './alert';

const publicComponents = [
    TableComponent,
    TableCellComponent,
    TableRowComponent,
    ButtonComponent,
    CartComponent,
    CartItemComponent,
    ImageComponent,
    ImageLoaderComponent,
    LinkComponent,
    AlertComponent
];

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        publicComponents
    ],
    declarations: [
        publicComponents
    ],
    providers: [],
})
export class ControlsModule { }
