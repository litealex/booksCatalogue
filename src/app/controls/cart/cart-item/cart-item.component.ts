import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
    selector: 'bc-cart-item',
    templateUrl: 'cart-item.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        class: 'bc-cart-item'
    }
})

export class CartItemComponent implements OnInit {
    @Input()
    label: string;
    constructor() { }

    ngOnInit() { }
}