import { Component, OnInit, ChangeDetectionStrategy, } from '@angular/core';
import { hostElement } from '@angular/core/src/render3/instructions';

@Component({
    selector: 'bc-cart',
    templateUrl: 'cart.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        class: 'bc-cart bc-cart_default'
    }
})

export class CartComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}