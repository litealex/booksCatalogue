import {
    Component,
    OnInit,
    ViewChild,
    Input,
    ChangeDetectionStrategy,
} from '@angular/core';

@Component({
    selector: 'bc-image',
    templateUrl: 'image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ImageComponent implements OnInit {
    @Input()
    width = 200;
    @Input()
    value: string;

  

    constructor(
    ) { }

    ngOnInit() {

    }


}