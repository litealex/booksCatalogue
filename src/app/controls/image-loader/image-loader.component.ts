import {
    Component,
    OnInit,
    ViewChild,
    forwardRef
} from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR
} from '@angular/forms';

@Component({
    selector: 'bc-image-loader',
    templateUrl: 'image-loader.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ImageLoaderComponent),
            multi: true
        }
    ]
})
export class ImageLoaderComponent implements OnInit, ControlValueAccessor {
    value: string;
    onChange = _ => { };
    onTouched = () => { };

    writeValue(obj: any): void {
        this.value = obj;
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {

    }
    setDisabledState?(isDisabled: boolean): void {

    }
    constructor() { }

    ngOnInit() { }

    handleChange(e: any) {
        let file = e.srcElement.files && e.srcElement.files[0];
        if (!file) {
            return null;
        }

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.onChange(e.target.result);
            this.value = e.target.result;
        }

        reader.readAsDataURL(file);
    }
}