import { IAuthor } from './author';
export interface IBook {
    title: string;
    authors: IAuthor[];
    pages: number;
    publisherName?: string;
    publicationYear?: number;
    releaseAt?: string;
    isbn?: string;
    images?: string[]
}

export interface IPersistedBook extends IBook {
    id: number;
}