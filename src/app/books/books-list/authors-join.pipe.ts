import { Pipe, PipeTransform } from '@angular/core';
import { IAuthor } from '../../models';

@Pipe({
    name: 'bcAuthorsJoin',
    pure: true
})

export class JoinPipe implements PipeTransform {
    transform(value: IAuthor[]): any {
        if (value == null) {
            return null;
        }

        return value
            .map(x => `${x.firstName} ${x.lastName}`)
            .join(', ');

    }
}