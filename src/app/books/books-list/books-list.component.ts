import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from '../books.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { IPersistedBook } from '../../models';

@Component({
    selector: 'bc-books-list',
    templateUrl: 'books-list.component.html'
})
export class BooksListComponent implements OnInit {
    books$: Observable<IPersistedBook>;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private booksService: BooksService
    ) { }

    ngOnInit() {
        this.books$ = this.route.data.pipe(
            map(x => x.books)
        );
    }

    deleteBook(book: IPersistedBook) {
        this.booksService.removeBook(book.id);
        this.router.navigate([]);
    }

}