import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { IPersistedBook } from '../../models';
import { BooksService } from '../books.service';

@Injectable()
export class BooksListResolverService implements Resolve<IPersistedBook[]> {

    constructor(private booksService: BooksService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IPersistedBook[] {
        return this.booksService.search(route.queryParams.sort);
    }
}