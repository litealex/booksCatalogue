import { ValidatorFn } from '@angular/forms';

export const isbn: ValidatorFn = (control) => {
    const value = control.value;
    if (value == null || value == '') {
        return null;
    }

    if (isbnCheck(value)) {
        return null;
    }

    return { isbn: true };
}

const isbnCheck = (isbn: string) => {
    isbn = isbn.replace(/[^\dX]/gi, '');
    if (isbn.length != 10) {
        return false;
    }
    let chars: any[] = isbn.split('');
    if (chars[9].toUpperCase() == 'X') {
        chars[9] = 10;
    }
    let sum = 0;
    for (let i = 0; i < chars.length; i++) {
        sum += ((10 - i) * parseInt(chars[i]));
    };
    return ((sum % 11) == 0);
}

