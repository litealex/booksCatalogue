import { ValidatorFn } from '@angular/forms';

export const minDate = (date: Date): ValidatorFn => {
    return (control) => {
        if (!control.value) {
            return null
        }
        return date > new Date(control.value)
            ? { minDate: true }
            : null;
    };
}