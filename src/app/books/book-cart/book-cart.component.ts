import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormArray
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { BooksService } from '../books.service';

import { IPersistedBook } from '../../models';
import * as bcValidators from './validators';



@Component({
    selector: 'bc-book-cart',
    templateUrl: 'book-cart.component.html'
})

export class BookCartComponent implements OnInit {
    form: FormGroup;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private booksService: BooksService
    ) { }

    get authors() {
        return this.form.get('authors') as FormArray;
    }

    ngOnInit() {
        this.form = this.fb.group({
            id: [],
            title: [null, [
                Validators.required,
                Validators.maxLength(30)]],
            authors: [],
            pages: [null, [
                Validators.required,
                Validators.min(1),
                Validators.max(10000)]],
            publisherName: [null, Validators.maxLength(30)],
            publicationYear: [null, Validators.min(1800)],
            releaseAt: [null, bcValidators.minDate(new Date('1800-01-01'))],
            isbn: [null, bcValidators.isbn],
            image: []
        });

        this.route.data.pipe(
            map(x => x.book)
        ).subscribe(x => {
            this.initForm(x);
        });
    }

    save() {
        this.markAsTouched();
        if (this.form.invalid) {
            return;
        }
        this.booksService.save(this.form.value.id, this.form.value);
        this.router.navigate(['/']);
    }

    appendAuthor() {
        this.authors.push(this.createAuthorFormGroup());
    }
    removeAuthor(id: number) {
        this.authors.removeAt(id);
    }

    initForm(book: IPersistedBook) {
        const authors = (book.authors || []).map(x => this.createAuthorFormGroup());
        this.form.setControl('authors', this.fb.array(authors, bcValidators.arrayAny));
        this.form.reset(book);
    }
    private markAsTouched() {
        Object.keys(this.form.controls).forEach(x => {
            this.form.controls[x].markAsTouched();
        });
    }

    private createAuthorFormGroup() {
        return this.fb.group({
            firstName: ['', [Validators.required, Validators.maxLength(20)]],
            lastName: ['', [Validators.required, Validators.maxLength(20)]]
        });
    }
}