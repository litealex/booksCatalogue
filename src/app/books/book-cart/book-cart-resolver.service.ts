import { Injectable } from '@angular/core';
import { IPersistedBook } from '../../models';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BooksService } from '../books.service';

@Injectable()
export class BookCartResolverService implements Resolve<IPersistedBook> {
    constructor(private booksService: BooksService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IPersistedBook {
        let id = route.paramMap.get('id');
        if (id == 'new') {
            return {
                id: null,
                authors: null,
                pages: null,
                title: null
            };
        }
        return this.booksService.getBook(+id);
    }
}