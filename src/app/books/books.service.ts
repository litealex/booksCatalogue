import { Injectable } from '@angular/core';
import { StorageService } from '../infrastructure';
import { IBook, IPersistedBook } from '../models';

const keys = {
    books: 'books',
    id: 'id'
};

@Injectable()
export class BooksService {

    constructor(private storageService: StorageService) { }

    search(sortBy: keyof IBook | null): any {
        const books = this.getPersistedBooks();
        if (sortBy == null) {
            return books;
        }

        return books.sort((a, b) => {
            const f1 = a[sortBy],
                f2 = b[sortBy];
            if ((f1 > f2)) {
                return 1
            } else {
                return -1;
            }
        });
    }

    getBook(id: number): IPersistedBook {
        const books = this.getPersistedBooks();
        const book = books.find(x => x.id == id);
        if (!book) {
            throw new Error(`book ${id} not found`);
        }
        return book;
    }

    save(id: number | null, book: IBook): number {
        let persistedBooks: IPersistedBook[];

        if (id == null) {
            persistedBooks = this.insert(book);
        } else {
            persistedBooks = this.update({ ...book, id });
        }

        this.storageService.saveItem(keys.books, persistedBooks);
        return id;

    }

    removeBook(id: number) {
        let books = this.getPersistedBooks().filter(x => x.id !== id);
        this.storageService.saveItem(keys.books, books);
    }

    private insert(book: IBook) {
        const id = this.getNextId();
        let books = this.getPersistedBooks();
        return [...books, { ...book, id }];
    }
    private update(book: IPersistedBook) {
        return this.getPersistedBooks().map(x => {
            if (x.id === book.id) {
                return book;
            }
            return x;
        });
    }

    private getPersistedBooks() {
        return this.storageService.getItem<IPersistedBook[]>(keys.books) || [];
    }

    private getNextId(): number {
        const cid = this.storageService.getItem<number>(keys.id) || 0;
        return this.storageService.saveItem(keys.id, cid + 1);
    }

}