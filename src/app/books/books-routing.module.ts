import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksListComponent } from './books-list';
import { BookCartComponent } from './book-cart';
import { BooksListResolverService } from './books-list/books-list-resolver.service';
import { BookCartResolverService } from './book-cart/book-cart-resolver.service';



const routes: Routes = [
    {
        path: '',
        runGuardsAndResolvers: 'always',
        component: BooksListComponent,
        resolve: {
            books: BooksListResolverService
        }
    },
    {
        path: 'book/:id',
        component: BookCartComponent,
        resolve: {
            book: BookCartResolverService
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        BooksListResolverService,
        BookCartResolverService
    ]
})
export class BooksRoutingModule { }
