import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookCartComponent } from './book-cart';
import { BooksListComponent } from './books-list';
import { BooksService } from './books.service';
import { BooksRoutingModule } from './books-routing.module';
import { ControlsModule } from '../controls';
import { ReactiveFormsModule } from '@angular/forms';
import { JoinPipe } from './books-list';


@NgModule({
    imports: [
        ControlsModule,
        BooksRoutingModule,
        CommonModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        BookCartComponent,
        BooksListComponent,
        JoinPipe
    ],
    providers: [
        BooksService
    ],
})
export class BooksModule { }
